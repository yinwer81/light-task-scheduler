package com.github.ltsopensource.example.springboot;

import com.github.ltsopensource.example.support.MasterChangeListenerImpl;
import com.github.ltsopensource.spring.boot.annotation.MasterNodeListener;

/**
 * @author Robert HG (254963746@qq.com) on 4/9/16.
 */
@MasterNodeListener
public class MasterNodeChangeListener extends MasterChangeListenerImpl {
}
